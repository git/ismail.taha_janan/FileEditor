/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package fileeditor;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author istahajana
 */
public class FileEditor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Printer prnt = new Printer();
        Reader reader = new Reader();
        
        
        prnt.printMenu();
        
        String menu = reader.readMenu();
        String fileName = reader.readFileName();
        
        
        
        switch (menu) {
            case "N" -> {
                try {
                    File myFile = new File(fileName+".txt");
                    if (myFile.createNewFile()) {
                        prnt.printMsg("File created: " + myFile.getName());
                    } else {
                        prnt.printMsg("File already exists.");
                    }
                } catch (IOException e) {
                    prnt.printMsg("ERROR : "+ e);
                }
            }

            
            case "O" -> {
                
            }
             
            case "S" -> {
                File myFile = new File(fileName+".txt");
                if (myFile.delete()) {
                    prnt.printMsg("Deleted the file: " + myFile.getName());
                } else {
                    prnt.printMsg("Failed to delete the file.");
                }
            }
            default -> throw new AssertionError();
        }
        
    }
    
}
