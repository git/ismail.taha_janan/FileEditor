/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fileeditor;

import java.util.Scanner;

/**
 *
 * @author istahajana
 */
public class Reader {
    Printer prnt = new Printer();
    Scanner sc = new Scanner(System.in);
    
    public String readMenu(){
        while (true) { 
            String numMenu = sc.nextLine();
            if ("N".equals(numMenu) || "O".equals(numMenu) || "S".equals(numMenu)) {
                return numMenu;
            }else prnt.printMsg("Entrez la bonne valeur SVP:");
        }
    }
    
    public String readFileName(){
        prnt.printMsg("le nom du fichier");
        return sc.nextLine();
    }
}
